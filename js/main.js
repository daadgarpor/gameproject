const canvas = document.querySelector('canvas');
const ctx = canvas.getContext('2d');
let frames = 0;
let interval;
let enemies = [];
let balas = [];

let disparos =[];
let disparos2 =[];


let boxes = [];
let waveTime = 0;
/**
 * Funcion que limpia el canvas
 * con clearRect
 */
function clearCanvas() {
    ctx.clearRect(0,0,canvas.width, canvas.height);
    
  }


function checkCollisionBorders(obj){
    //Si en el punto x del brawl esta
    if(obj.x <= 0) {
        obj.x = 0;
      } else if (obj.x + obj.width >= canvas.width) {
        obj.x = canvas.width-obj.width;
      }

      if(obj.y + obj.height > canvas.height){
          obj.y= canvas.height- obj.height;
      }else if( obj.y <= 0){
            obj.y = 0;
      }
  }
  
/**
 * @function chasear()
 * Persigue al brawl a su posicion
 * Disminuyendo o  aumentando la distacia que hay entre ellos
 */  
function chasear(){
  brawl.x>enemigo.x ? enemigo.vx++ : enemigo.vx--;
  brawl.y>enemigo.y ? enemigo.vy++ :enemigo.vy--;
  enemigo.draw();
  enemigo.x = enemigo.vx;
  enemigo.y = enemigo.vy;
}



function chasear2(){
  brawl2.x>enemigo2.x ? enemigo2.vx++ : enemigo2.vx--;
  brawl2.y>enemigo2.y ? enemigo2.vy++ :enemigo2.vy--;
  enemigo2.draw();
  enemigo2.x = enemigo2.vx;
  enemigo2.y = enemigo2.vy;
}

function checkCollisionEnemyVsBrawl(){
    for(var i in enemigo){
      var disparo = enemigo[i];
      let choco= checkCollisionBrawlVsPlataformas(brawl,disparo);
  
      if(choco=='left' || choco=='right' || choco=='down' || choco=='up'){
       gameOver();
      }
    }
  
}

/**
 * @function laberintoAleatorio()
 * Dibuja las boxes dejando un espacio de 190 entra cada una de ellas
 */
function laberintoAleatorio(){
  let platform_width = 70;
  let platform_height = 70;
  let espacio = 80;

  for (let i = 0; i < 4 ; i++) {
    boxes.push({ x: espacio, y: 80,width: platform_width,height: platform_height})
    boxes.push({ x: espacio, y: 270, width: platform_width, height: platform_height})
    boxes.push({ x: espacio, y: 460, width: platform_width, height: platform_height})
    espacio += 190;
  }

  drawBoxes();
}

function drawBoxes() {
  let img = new Image();
  img.src = "./images/cuadros.png";

  boxes.map(box =>
    ctx.drawImage( img,(0*174)/4,(0*64)/ 4,288/6, 192/4,box.x, box.y, box.width,box.height))
}

/**
 * @function checkCollisionBrawlVsPlataformas(braw, box)
 * @param {braw} braw recibe el obj brawl
 * @param {box} box recibe a boxes 
 * 
 * Checar bordes de la caja por los 4 lados 
 * @returns direction
 */
function checkCollisionBrawlVsPlataformas(braw, box) {
  // calcula la posicion donde se encuentra con el ancho y la altura de los objetos
  const vectorX = braw.x + braw.width / 2 - (box.x + box.width / 2)
  //console.log(vectorX)
  const vectorY = braw.y + braw.height / 2 - (box.y + box.height / 2)
  //console.log(vectorY)
  //calcula la mitad del ancho del objeto
  const halfWidths = braw.width / 2 + box.width / 2
  const halfHeights = braw.height / 2 + box.height / 2

  //regresa la direcion condicionando los vectores y la mitades de los objetos
  let collisionDirection = null;

  //choca por los 4 lados
  // Valor Absoluto para los calculos que sale negativos
  if (Math.abs(vectorX) < halfWidths && Math.abs(vectorY) < halfHeights) {
    var offsetX = halfWidths - Math.abs(vectorX)
    var offsetY = halfHeights - Math.abs(vectorY)
    if (offsetX < offsetY) {
      if (vectorX > 0) {
        collisionDirection = 'left';
        braw.x += offsetX;
      } else {
        collisionDirection = 'right';
        braw.x -= offsetX;
      }
    } else {
      if (vectorY > 0) {
        collisionDirection = 'top';
        braw.y += offsetY;
      } else {
        collisionDirection = 'bottom';
        braw.y -= offsetY;
      }
    }
  }
  return collisionDirection;
}


function gameOver(){
  clearInterval(interval);
  var img = new Image();
  img.src = './images/loser.png';
  img.onload = function(){
     ctx.drawImage(img, canvas.width/4, canvas.height/3, 500, 300);
  }
}

function winner(){
  clearInterval(interval);
  var img = new Image();
  img.src = './images/winner.png';
  img.onload = function(){
     ctx.drawImage(img, canvas.width/4, canvas.height/3, 500, 300);
  }
}


/**
 * @function checkBoxes
 * Funcion para checar colision entre brawles y la la caja
 * Checa la dirección por donde choca 
 */
function checkBoxes(){
  boxes.map(boxElement => {
    checkCollisionBrawlVsPlataformas(brawl, boxElement);
  });
}

function checkBoxes2(){
  boxes.map(boxElement => {
    checkCollisionBrawlVsPlataformas(brawl2, boxElement);
  });
}

function moverDisparos(){
  for(var i in disparos){
    var disparo = disparos[i];

    if(brawl.direction == 'down'){
      disparo.y += 60;
    }

    if(brawl.direction == 'up'){
      disparo.y -= 60;
      
    }
    if(brawl.direction == 'right'){
      disparo.x += 60;
      
    }
    if(brawl.direction == 'left'){
      disparo.x -= 60;
      
    }
// elimina dispara di sale del canvas
    if (
      disparo.x > canvas.width ||
      disparo.x < 0 ||
      disparo.y > canvas.height ||
      disparo.y < 0
    )
      return disparos.splice(i, 1); 
  
  }
}

function DrawDisparos(){
  for(var i in disparos){
    var disparo = disparos[i];
    ctx.fillStyle = 'black';
    ctx.fillRect(disparo.x,disparo.y,disparo.width,disparo.height); 
  }
}

function balass(){
  disparos.push({
    x: brawl.x + 35,
    y: brawl.y + 35,
    width: 8,
    height: 8
  });
}



//Balas2

function moverDisparos2(){
  for(var i in disparos2){
    var disparo = disparos2[i];

    if(brawl2.direction == 'down'){
      disparo.y += 60;
    }

    if(brawl2.direction == 'up'){
      disparo.y -= 60;
      
    }
    if(brawl2.direction == 'right'){
      disparo.x += 60;
      
    }
    if(brawl2.direction == 'left'){
      disparo.x -= 60;
      
    }
// elimina dispara di sale del canvas
    if (
      disparo.x > canvas.width ||
      disparo.x < 0 ||
      disparo.y > canvas.height ||
      disparo.y < 0
    )
      return disparos2.splice(i, 1); 
  
  }
}

function DrawDisparos2(){
  for(var i in disparos2){
    var disparo = disparos2[i];
    ctx.fillStyle = 'black';
    ctx.fillRect(disparo.x,disparo.y,disparo.width,disparo.height); 
  }
}

function balass2(){
  disparos2.push({
    x: brawl2.x + 35,
    y: brawl2.y + 35,
    width: 8,
    height: 8
  });
}

function checkCollisionBalasVsEnemy2(){
  let damage = 10;
  
  for(var i in disparos2){
    var disparo = disparos2[i];
    let choco= checkCollisionBrawlVsPlataformas(disparo,enemigo2);

    if(choco=='left' || choco=='right' || choco=='down' || choco=='up'){
      enemigo2.receiveDamage(damage);
      
    }
    //disparos.splice(i, 1); 
    console.log("Vida Enemy: ",enemigo2.health-damage);
    let vidaRestante1 = enemigo2.health-damage;
    ctx.font = '20px Arial';
    ctx.fillStyle = 'black';
    ctx.fillText("Player 2: ", 150, canvas.height-575);
    ctx.fillText("Vida Fantasma: ",150, canvas.height-550);
    ctx.fillText(enemigo2.health,300, canvas.height-550);
    ctx.fillText(vidaRestante1,150, canvas.height-525);
  }

  if(enemigo2.health <= 0){
    winner();
  }

}



/**
 * @function start
 * inicia el juego, se manda a llamar la funcion main y se invoca al presionar la tecla enter
 */
function start() {
  if(interval) return;
  interval = setInterval(main, 1000/70);
}

/**
 * @function loadScreen
 * muestra la imagen del principio se manda a llamar en la clase de Background 
 */
function loadScreen() {
  const logo = new Image();
  logo.src = "./images/finalFondo.png" ;
  logo.onload = () => ctx.drawImage(logo, 0, 0, canvas.width, canvas.height);
}

/**
 * @function 
 * checa la colision entre balas y fantasma
 * por cada disparo en el array checa si choca con el fantasma
 */
function checkCollisionBalasVsEnemy(){

  for(var i in disparos){
    var disparo = disparos[i];
    let choco= checkCollisionBrawlVsPlataformas(disparo,enemigo);

    if(choco=='left' || choco=='right' || choco=='down' || choco=='up'){
      let damage = 10;
      enemigo.receiveDamage(damage);

      let vidaRestante = enemigo.health-damage;
      ctx.font = '20px Arial';
      ctx.fillStyle = 'black';
      ctx.fillText("Player 1: ",canvas.width/2, canvas.height-575);
      ctx.fillText("Vida Fantasma: ",canvas.width/2, canvas.height-550);
      ctx.fillText(enemigo2.health,canvas.width/1.5, canvas.height-550);
      ctx.fillText(vidaRestante,canvas.width/2, canvas.height-525);
    }
    //disparos.splice(i, 1); 
  

  }

  if(enemigo.health <= 0){
    winner();
  }

}

/**
 * Funcion Principal
 *
 */
function main() {
    frames++;
    clearCanvas();
    // Background
    background.draw();
    laberintoAleatorio();
    
    // Crear Brawl
    
    brawl.draw();

    brawl.x += brawl.vx; 
    brawl.y += brawl.vy;

    //brawl2
    brawl2.draw();
    brawl2.x += brawl2.vx; 
    brawl2.y += brawl2.vy;
  
    // Crear Enemigo
  
    enemigo.x += enemigo.vx; 
    enemigo.y += enemigo.vy;

    //Falta que se active despues de un tiempo
  
    chasear();

    //enemigo2

    enemigo2.x += enemigo2.vx; 
    enemigo2.y += enemigo2.vy;

    chasear2();

    checkCollisionBorders(brawl2);
    checkCollisionBorders(enemigo2);
    checkBoxes2();
    //checkCollisionEnemyVsBrawl();

 // Checar border
     checkCollisionBorders(brawl);
     checkCollisionBorders(enemigo);
     checkBoxes();    
  //  checkCollisionEnemyVsBrawl();
  
  //Disparos
  moverDisparos();
  DrawDisparos();
  checkCollisionBalasVsEnemy();

  //Disparas brwal2
  moverDisparos2();
  DrawDisparos2();
  checkCollisionBalasVsEnemy2();
  }

 



