class Background {
    constructor() {
      this.x = 0;
      this.y = 0;
      this.position = 0;
      this.animate = 0;
      this.width = canvas.width;
      this.height = canvas.height;
      this.img = new Image();
      this.img.src = "./images/fondoPrincipal.jpg";
      this.img.onload = () =>{
        this.draw();
        loadScreen();
        
      }
    }
    draw(){
      ctx.drawImage(
        this.img,
        this.x,
        this.y,
        this.width,
        this.height
      )
    }
  }

  class Laberinto {
    constructor() {
      this.x = 0;
      this.y = 0;
      this.width = 100;
      this.height = 100;
      this.position = 0;
      this.animate = 0;
      this.img = new Image();
      this.img.src = "./images/cuadros.png";
    }
  //176 *64
    draw(x,y){
      ctx.drawImage(
        // imagen fuente
        this.img,
        // posicion de la x de la sub-imagen (sx)
        (this.animate * 174)/4,
        // posicion de la y de la sub-imagen (sy)
        (this.position * 64)/ 4,
        // ancho de la sub-imagen (sw)
        288/6,
        // alto de la subimagen (sh)
        192/4,
        x,
        y,
        this.width,
        this.height
      )   
    }
  }

  class Brawl {
    constructor(){
      this.width = 70;
      this.height = 70;
      this.y = canvas.height-this.height;
      this.x = canvas.width-this.width; 
      this.vx = 0;
      this.vy = 0;
      this.direction = undefined;
      this.animate = 0; 
      this.position = 0;
      this.img = new Image();
      this.img.src = "./images/soldado.png";
      this.damage = 10;
    
    }
      draw() {
        //pinta en la posicion 
        ctx.drawImage(
          // imagen fuente
          this.img,
          // posicion de la x de la sub-imagen (sx)
          (this.animate * 288)/ 6,
          // posicion de la y de la sub-imagen (sy)
          (this.position * 192)/ 4,
          // ancho de la sub-imagen (sw)
          288/6,
          // alto de la subimagen (sh)
          192/4,
          this.x,
          this.y,
          this.width,
          this.height
        )
      }

      moveLeft() {
        this.vx -=3;
        this.position = 1;
      }
      moveRight() {
        this.vx += 3;
        this.position = 3;
      }

      moveDown() {
        this.vy += 3;
        this.position = 0;
      }
      moveUp() {  
        this.vy -= 3;
        this.position = 2;
      }
    }

    class Brawl2 {
      constructor(){
        this.width = 70;
        this.height = 70;
        this.y = canvas.height-this.height;
        this.x = canvas.width-this.width; 
        this.vx = 0;
        this.vy = 0;
        this.direction = undefined;
        this.animate = 0; 
        this.position = 0;
        this.img = new Image();
        this.img.src = "./images/soldado2.png";
        this.damage = 10;
      
      }
        draw() {
          //pinta en la posicion 
          ctx.drawImage(
            // imagen fuente
            this.img,
            // posicion de la x de la sub-imagen (sx)
            (this.animate * 288)/ 6,
            // posicion de la y de la sub-imagen (sy)
            (this.position * 192)/ 4,
            // ancho de la sub-imagen (sw)
            288/6,
            // alto de la subimagen (sh)
            192/4,
            this.x,
            this.y,
            this.width,
            this.height
          )
        }
  
        moveLeft() {
          this.vx -=3;
          this.position = 1;
        }
        moveRight() {
          this.vx += 3;
          this.position = 3;
        }
  
        moveDown() {
          this.vy += 3;
          this.position = 0;
        }
        moveUp() {  
          this.vy -= 3;
          this.position = 2;
        }
      }
  

    class Enemy{
      constructor() {
        this.width = 70;
        this.height = 70;
        this.x = 600;
        this.y = 600;
        this.vx = 0;
        this.vy = 0; 
        this.animate = 0; 
        this.position = 1;
        this.image = new Image();
        this.image.src = "./images/1_3_5.png" ;
        this.health = 100;
        this.playerPurse = 1;
      }
      draw() {
        //625*300
        ctx.drawImage(
          // imagen fuente
          this.image,
          // posicion de la x de la sub-imagen (sx)
          (this.animate * 625)/ 7,
          // posicion de la y de la sub-imagen (sy)
          (this.position * 300)/ 3,
          // ancho de la sub-imagen (sw)
          625/5,
          // alto de la subimagen (sh)
          300/4,
          this.x,
          this.y,
          this.width,
          this.height
        );
      }

      receiveDamage(damage) {
        return (this.health -= damage);
      }

      
    }