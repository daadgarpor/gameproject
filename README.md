# GameProject  NOOBRAWL
==========================

<img src="images/nuevoPrincipal.png">

Proyecto desarrollado por Daniel Garcia Portillo con html, canvas y javaScript.
Single o 2 Jugadores

**!!! Estructura de Archivos**
---------------
* **Motor de juego**
  * main.js
  * images - Carpeta
  * class.js
  * instances.js
  * index.html  

## Acerca del juego

### Logica de juego.

En un entorno como laberinto el jugador o jugadores se pueden mover con las teclas de flechas y 
dispara en direccion a su enemigo el fantasma. El fantasma se muevera por laberinto buscando al brawl. Gana si el jugador termina con la vida de fantasma.


## Por hacer
 * Corregir colision entre brawls y fantasma
 * Que el tamaño de mapa/entorno se achique para provacar enfrentamiento
 * Que en centro o aleatoriammente aparescan monedas.
 * Gana quien junte 10 gemas o
 * Gana quien mate al contrincante.
 * Tres Vidas - Revive.
 * Establecer tiempo de fin de juego.

**Jugadore podrian configuracion:** 
 * con fantasma
 * con tiempo
 * con monedas

Credits:
* sprides conseguidos en páginas gratis:
  * https://craftpix.net/freebies/ 
  * https://itch.io/game-assets/free

* Laberinto  https://cubic-tree-graphics.itch.io/six-grounds-assets-pack
* Fondo https://craftpix.net/freebies/free-desert-scrolling-2d-game-backgrounds/

* funcion para checar todos bordes project - https://gitlab.com/kavak-internship-program/project-1-demo-3/-/blob/master/main.js

* Disparos https://www.youtube.com/watch?v=JORxRP9hR3s

    



